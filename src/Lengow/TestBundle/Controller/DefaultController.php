<?php

namespace Lengow\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\DBAL\DBALException;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\Request;
use Lengow\TestBundle\Entity\Order;

class DefaultController extends Controller
{
    /**
     * Display the list of orders and a form to add an order
     * @param Request $request
     * @return string
     */
    public function indexAction(Request $request)
    {
        // Get entity manager
        $entityManager = $this->container->get('doctrine.orm.default_entity_manager');

        // Retreive orders
        $orderSerializer = $this->container->get('lengow_test');
        $orders = $orderSerializer->getEntities();

        // Persist entities in database
        foreach ($orders as $order) {
            try {
                $entityManager->persist($order);
                $entityManager->flush();
            } catch (DBALException $e) {
                // Ignore unique constraint violation silently
                if( $e->getPrevious()->getCode() === '23000' )
                {
                    $this->container->get('logger')->addInfo('Did not insert order with id '.$order.' into database. Already there!');
                    $this->getDoctrine()->resetManager();
                    $entityManager = $this->container->get('doctrine.orm.default_entity_manager');
                } else {
                    throw $e;
                }
            }
        }

        // Handle request
        $order = new Order();
        $form = $this->createForm('form_order_type', $order, array(
            'action' => $this->generateUrl('homepage'),
            'method' => 'POST',
        ));
        $form->handleRequest($request);

        // Save order if the form is valid
        if ($form->isValid()) {
            $entityManager->persist($order);
            $entityManager->flush();
        }

        // Creates a simple grid based on your entity (ORM)
        $source = new Entity('LengowTestBundle:Order');

        // Get a Grid instance
        $grid = $this->get('grid');

        // Attach the source to the grid
        $grid->setSource($source);

        // Return the response of the grid to the template
        return $grid->getGridResponse('LengowTestBundle:Default:index.html.twig', array('form' => $form->createView()));
    }
}
