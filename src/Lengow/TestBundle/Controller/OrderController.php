<?php

namespace Lengow\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends Controller
{
    /**
     * Get all orders in JSON format
     * @param Request $request
     * @return string
     */
    public function getAllAction(Request $request)
    {
        $entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        $serializer = $this->container->get('serializer');

        // Get YAML parameter if it exists
        $isYaml = !empty($request->query->get('yaml')) && ($request->query->get('yaml') == true) ? true : false;

        // Get all orders and serialize them into JSON
        $orders = $entityManager->getRepository('LengowTestBundle:Order')->findAll();
        $orders = $serializer->serialize($orders, 'json');

        // Create JSON with some metadatas
        $responseContent = '{'
                . '"status": 200, '
                . '"message": "OK", '
                . '"datas": '.$orders.''
        .'}';

        // Convert to yaml format if needed
        if ($isYaml) {
            $dumper = new Dumper();
            $responseContent = $dumper->dump($serializer->decode($responseContent, 'json'), 4);
        }

        // create a response
        $response = new Response($responseContent);
        if ($isYaml) {
            $response->headers->set('Content-Type', 'application/x-yaml');
        } else {
            $response->headers->set('Content-Type', 'application/json');
        }
        return $response;
    }

    /**
     * Get a specific order in JSON format
     * @param Request $request
     * @param int îdOrder
     * @return string
     */
    public function getAction(Request $request, $idOrder)
    {
        $entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        $serializer = $this->container->get('serializer');
        $statusCode = 200;

        // Get YAML parameter if it exists
        $isYaml = !empty($request->query->get('yaml')) && ($request->query->get('yaml') == true) ? true : false;

        // Get order
        $order = $this->getDoctrine()->getManager()->find('LengowTestBundle:Order', $idOrder);

        if (empty($order)) {
            // Create JSON with some metadatas
            $statusCode = 404;
            $responseContent = '{'
                    . '"status": '.$statusCode.', '
                    . '"message": "Not found"'
            .'}';
        } else {
            $order = $serializer->serialize($order, 'json');
                    // Create JSON with some metadatas
            $responseContent = '{'
                    . '"status": '.$statusCode.', '
                    . '"message": "OK", '
                    . '"datas": '.$order.''
            .'}';
        }

        // Convert to yaml format if needed
        if ($isYaml) {
            $dumper = new Dumper();
            $responseContent = $dumper->dump($serializer->decode($responseContent, 'json'), 2);
        }

        $response = new Response($responseContent, $statusCode);
        if ($isYaml) {
            $response->headers->set('Content-Type', 'application/x-yaml');
        } else {
            $response->headers->set('Content-Type', 'application/json');
        }
        return $response;
    }
}
