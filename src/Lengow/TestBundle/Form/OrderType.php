<?php

namespace Lengow\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idFlux')
            ->add('idOrder')
            ->add('marketplace')
            ->add('purchaseDate')
            ->add('save', 'submit', array('label' => 'Add order'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lengow\TestBundle\Entity\Order',
            'validation_groups' => array('orderType'),
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'intention' => 'order_intention',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_order_type';
    }
}
