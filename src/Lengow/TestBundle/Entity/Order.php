<?php

namespace Lengow\TestBundle\Entity;

use Lengow\TestBundle\Entity\EntityInterface\OrderInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`order`", options={"comment"="Orders"}, uniqueConstraints={@ORM\UniqueConstraint(name="UNQ_idFlux_idOrder", columns={"idFlux", "idOrder"})})
 */
class Order implements OrderInterface
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Identifier of a flux
     * @var int
     * @ORM\Column(name="idFlux", type="integer", options={"comment"="Identifier of a Flux", "unsigned"=true})
     */
    protected $idFlux;

    /**
     * Identifier of an order
     * @var string
     * @ORM\Column(name="idOrder", type="string", options={"comment"="Identifier of an Order"})
     */
    protected $idOrder;

    /**
     * Marketplace from where the order comes
     * @var string
     * @ORM\Column(name="marketplace", type="string", options={"comment"="Marketplace from where the order comes"})
     */
    protected $marketplace;

    /**
     * Purchase date of the order
     * @var \Datetime
     * @ORM\Column(name="purchaseDate", type="datetime", options={"comment"="Purchase date of the order"})
     */
    protected $purchaseDate;


    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    public function __toString()
    {
        return $this->getIdFlux().' - '.$this->getIdOrder();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return OrderInterface
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;

        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer 
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set idOrder
     *
     * @param string $idOrder
     * @return OrderInterface
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;

        return $this;
    }

    /**
     * Get idOrder
     *
     * @return string
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return OrderInterface
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set purchaseDate
     *
     * @param \DateTime $purchaseDate
     * @return OrderInterface
     */
    public function setPurchaseDate($purchaseDate)
    {
        if (is_string($purchaseDate)) {
            $this->purchaseDate = new \Datetime($purchaseDate);
        } else {
            $this->purchaseDate = $purchaseDate;
        }
        
        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime 
     */
    public function getPurchaseDate()
    {

        return !empty($this->purchaseDate) ? $this->purchaseDate->format('d-m-Y') : null;
    }
}
