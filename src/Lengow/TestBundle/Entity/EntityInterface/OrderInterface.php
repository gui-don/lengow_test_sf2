<?php

namespace Lengow\TestBundle\Entity\EntityInterface;

interface OrderInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();


    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return OrderInterface
     */
    public function setIdFlux($idFlux);

    /**
     * Get idFlux
     *
     * @return integer
     */
    public function getIdFlux();

    /**
     * Set idOrder
     *
     * @param string $idOrder
     * @return OrderInterface
     */
    public function setIdOrder($idOrder);

    /**
     * Get idOrder
     *
     * @return string
     */
    public function getIdOrder();

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return OrderInterface
     */
    public function setMarketplace($marketplace);

    /**
     * Get marketplace
     *
     * @return string
     */
    public function getMarketplace();

    /**
     * Set purchaseDate
     *
     * @param \DateTime $purchaseDate
     * @return OrderInterface
     */
    public function setPurchaseDate($purchaseDate);

    /**
     * Get purchaseDate
     *
     * @return \DateTime
     */
    public function getPurchaseDate();

}
