<?php

namespace Lengow\TestBundle\Service;

use Lengow\TestBundle\Entity\EntityInterface\OrderInterface\OrderInterface;
use Lengow\TestBundle\Entity\Order;

class OrderRetreiver
{
    private $xmlFile;
    private $urlOrders;
    private $logger;

    public function __construct($urlOrders, \Monolog\Logger $logger)
    {
        $this->urlOrders = $urlOrders;
        $this->logger = $logger;
        $this->refresh();
    }

    public function __toString()
    {
        return $this->urlOrders;
    }

    /**
     * Get orders from the orders URL
     * @return bool False if an error occured
     */
    public function refresh()
    {
        $this->logger->addInfo('OrderSerializer try to load XML file from '.$this->urlOrders);

        $this->xmlFile = file_get_contents($this->urlOrders);

        if (!empty($this->xmlFile)) {
            $this->logger->addInfo('OrderSerializer successfully loaded XML file from '.$this->urlOrders);
            return true;
        } else {
            $this->logger->addError('OrderSerializer failed to load XML file from '.$this->urlOrders);
            return false;
        }
    }

    /**
     * Generate order entities from the source XML file
     * @return OrderInterface[]
     */
    public function getEntities()
    {
        $orderEntities = array();

        // Get trough the XML file
        $domDocument = new \DOMDocument('1.0', 'UTF-8');
        $domDocument->loadXML($this->xmlFile);

        // Deserialize XML file into order objects
        $i = 0;
        foreach ($domDocument->getElementsByTagName('order') as $order) {
            $orderEntities[$i] = new Order();
            
            foreach($order->childNodes as $orderElement) {
                switch ($orderElement->tagName) {
                    case 'marketplace':
                        $orderEntities[$i]->setMarketplace($orderElement->nodeValue);
                        break;
                    case 'idFlux':
                        $orderEntities[$i]->setIdFlux((int) $orderElement->nodeValue);
                        break;
                    case 'order_id':
                        $orderEntities[$i]->setIdOrder($orderElement->nodeValue);
                        break;
                    case 'order_purchase_date':
                        $orderEntities[$i]->setPurchaseDate($orderElement->nodeValue);
                        break;
                }
            }
            $i++;
        }

        return $orderEntities;
    }
}

