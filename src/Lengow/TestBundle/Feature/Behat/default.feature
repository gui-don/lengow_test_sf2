@Home
Feature: A user comes to the homepage

    Scenario Outline: Get to the homepage and see the table of orders
        When I go to "/"
        Then the response status code should be 200
        And the url should match "/"
        And I should see "<idOrder>" in the "#wrapper table tr:nth-child(<tr>)" element
        And I should see "<idFlux>" in the "#wrapper table tr:nth-child(<tr>)" element
        And I should see "<marketplace>" in the "#wrapper table tr:nth-child(<tr>)" element
        And I should see "<purchaseDate>" in the "#wrapper table tr:nth-child(<tr>)" element

        Examples:
            | tr | idOrder             | idFlux | marketplace | purchaseDate               |
            | 3  | 111-2222222-3333333 | 88827  | amazon      | Oct 21, 2014, 12:00:00 AM  |
            | 4  | 123-4567890-1112131 | 88827  | amazon      | Oct 20, 2014, 12:00:00 AM  |
            | 5  | 222-1234567-1547225 | 88827  | amazon      | Oct 21, 2014, 12:00:00 AM  |
            | 6  | 114KL55M            | 88829  | cdiscount   | Oct 22, 2014, 12:00:00 AM  |
            | 7  | 11FGZ4SQ            | 88829  | cdiscount   |  |

    Scenario: Get to the homepage and see the right table of orders
        When I go to "/"
        Then the response status code should be 200
        And the url should match "/"
        And I should see "Add an order" in the ".bootstrap-frm" element
        When I fill in "form_order_type[idFlux]" with "88827"
        When I fill in "form_order_type[idOrder]" with "444-5555555-6666666"
        When I fill in "form_order_type[marketplace]" with "amazon"
        When I select "2" from "form_order_type[purchaseDate][date][month]"
        When I select "15" from "form_order_type[purchaseDate][date][day]"
        When I select "2012" from "form_order_type[purchaseDate][date][year]"
        When I select "0" from "form_order_type[purchaseDate][time][hour]"
        When I select "20" from "form_order_type[purchaseDate][time][minute]"
        When I press "form_order_type_save"
        Then the response status code should be 200
        And the url should match "/"
        And I should see "444-5555555-6666666" in the "#wrapper table tr:nth-child(8)" element
        And I should see "88827" in the "#wrapper table tr:nth-child(8)" element
        And I should see "amazon" in the "#wrapper table tr:nth-child(8)" element
        And I should see "Feb 15, 2012, 00:20:00 AM" in the "#wrapper table tr:nth-child(8)" element
