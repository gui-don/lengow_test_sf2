<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Context\Context;
use Behat\Testwork\Hook\Scope\BeforeSuiteScope;
use Behat\Testwork\Hook\Scope\AfterSuiteScope;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Behat\MinkExtension\Context\MinkContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context object.
     * You can also pass arbitrary arguments to the context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * Init database with fixtures
     */
    protected static function initDatabase()
    {
        $kernel = new \AppKernel('test', true);
        $kernel->boot();

        $app = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
        $app->setAutoExit(false);

        self::runCommand($app, 'doctrine:database:create');
        self::runCommand($app, 'doctrine:schema:create');
    }

    /**
     * Drop database
     */
    protected static function dropDatabase()
    {
        $kernel = new \AppKernel('test', true);
        $kernel->boot();

        $app = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
        $app->setAutoExit(false);

        self::runCommand($app, 'doctrine:database:drop', array('--force' => true));
    }

    /**
     * Run console command
     * @param \Symfony\Bundle\FrameworkBundle\Console\Application $app
     * @param array $command
     * @param array $options
     * @return int 0 if everything went fine, or an error code
     */
    protected static function runCommand($app, $command, $options = array())
    {
        $options['--env'] = 'test';
        $options['--quiet'] = null;
        $options['--no-interaction'] = true;
        $options = array_merge($options, array('command' => $command));

        return $app->run(new \Symfony\Component\Console\Input\ArrayInput($options));
    }
}

/**
 * Behat context class.
 */
class LengowContext extends FeatureContext implements Context, SnippetAcceptingContext
{
    use KernelDictionary;

    /**
     * Initialize context
     * @param string $baseUrl
     */
    public function __construct()
    {

    }

    public function setKernel($kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @BeforeSuite
     */
    public static function setup(BeforeSuiteScope $scope)
    {
        self::initDatabase();
    }

    /**
     * @AfterSuite
     */
    public static function teardown(AfterSuiteScope $scope)
    {
        self::dropDatabase();
    }

    /**
     * @Then /^I should display the page$/
     */
    public function displayPage()
    {
        echo "<pre>";
        print_r(        $this->getSession()->getResponseHeaders()
);
        echo "</pre>";

    }

    /**
     * @Then /^I should have a content of type "(?P<type>(?:[^"]|\\")*)"$/
     */
    public function ShouldHaveContentType($type)
    {
        $headers = $this->getSession()->getResponseHeaders();

        if ($headers['content-type'][0] === $type) {
            return true;
        } else {
            throw new Exception("Response don’t have the excpected content type. Expected $type, got ".$headers['content-type'][0]." \n");
        }
    }

    /**
     * @When /^I submit the form "(?P<form>(?:[^"]|\\")*)"$/
     */
    public function submitTheForm($form)
    {
        $page = $this->getSession()->getPage();
        $element = $page->find('css',"form[name=\"$form\"] button");
        $element->click();
    }

    /**
     * @Given /^I don(?:'|’)t follow redirection$/
     * @When /^I should not follow redirection$/
     */
    public function iDontFollowTheRedirection()
    {
        $this->getSession()->getDriver()->getClient()->followRedirects(false);
    }


    /**
     * @When /^I follow the redirection$/
     * @Then /^I should be redirected$/
     */
    public function iFollowTheRedirection()
    {
        $client = $this->getSession()->getDriver()->getClient();
        $client->followRedirects(true);
        $client->followRedirect();
    }

    private function canIntercept()
    {
        $driver = $this->getSession()->getDriver();
        if (!$driver instanceof \Behat\Mink\Driver\GoutteDriver && !$driver instanceof Behat\Symfony2Extension\Driver\KernelDriver) {
            throw new UnsupportedDriverActionException('You need to tag the scenario with "@mink:goutte" or "@mink:symfony2". Intercepting the redirections is not supported by %s', $driver);
        }
    }
}
