@Order
Feature: A user comes to the /api/ page

    Scenario: Get to the /api/
        When I go to "/api/"
        Then the response status code should be 200
        And the url should match "/api/"
        Then I should have a content of type "application/json"
        Then the response should contain "88827"
        Then the response should contain "123-4567890-1112131"
        Then the response should contain "amazon"
        Then the response should contain "cdiscount"
        Then the response should contain "11FGZ4SQ"
        Then the response should contain "88829"

    Scenario: Get to the /api/ requesting yaml
        When I go to "/api/?yaml=true"
        Then the response status code should be 200
        And the url should match "/api/"
        Then I should have a content of type "application/x-yaml"

    Scenario: Get to the the /api/ page with a non existent order
        When I go to "/api/600/"
        Then the response status code should be 404
        And the url should match "/api/600/"
        Then I should have a content of type "application/json"

    Scenario: Get to the the /api/ page with a specific order
        When I go to "/api/2/"
        Then the response status code should be 200
        And the url should match "/api/2/"
        Then I should have a content of type "application/json"
        Then the response should contain "88827"
        Then the response should contain "123-4567890-1112131"
        Then the response should contain "amazon"

    Scenario: Get to the the /api/ page with a specific order with yaml
        When I go to "/api/4/?yaml=true"
        Then the response status code should be 200
        And the url should match "/api/4/"
        Then I should have a content of type "application/x-yaml"
        Then the response should contain "88829"
        Then the response should contain "cdiscount"
        Then the response should contain "114KL55M"